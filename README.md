# Clarinet

## Introdution

This repository contains the package build files to create a Debian package of Clarinet

## Links

[Source repository of the project](https://github.com/hirosystems/clarinet)  
[Release in this build v0.27.0](https://github.com/hirosystems/clarinet/releases/download/v0.27.0/clarinet-linux-x64-glibc.tar.gz)  

## License

[GPL-3.0](LICENSE)

